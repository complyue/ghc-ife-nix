# Nixpkgs overlay channel for GHC with interactive frontend support

This repository defines a Nixpkgs channel with an overlay to add &
set default a new `haskellPackages` with the GHC compiler from this
experimental branch:

	https://gitlab.haskell.org/complyue/ghc/tree/ghc-8.6-ife

## Usage

```shell
$ nix-channel --add https://gitlab.haskell.org/complyue/ghc-ife-nix/wikis/uploads/1ac1d0e5ac93abe8f08a39d2650ecd15 nixpkgs-overlays
$ 
$ nix-channel --list
nixpkgs https://nixos.org/channels/nixpkgs-unstable
nixpkgs-overlays https://gitlab.haskell.org/complyue/ghc-ife-nix/wikis/uploads/1ac1d0e5ac93abe8f08a39d2650ecd15
$ 
$ nix-channel --update nixpkgs-overlays
unpacking channels...
$ 
$ nix-shell -p 'ghc.withPackages (hs: [hs.cabal-install])'

# ...

[nix-shell:~]$ ghci
GHCi, version 8.6.5: http://www.haskell.org/ghc/  :? for help
Loaded GHCi configuration from /Users/cyue/.ghci
λ> :frontend
Could not find module ‘’
Use -v to see a list of the files searched for.
λ> 
```
